﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UnInfo.Controllers
{
    
    public class AdminController : Controller
    {
        [ActionName("login")]
        [HttpGet]
        public IActionResult Index()
        {
            if (HttpContext.Session.Get("username") != null) { return RedirectToAction("student", "Admin"); }
            return View("Index");
        }
        [ActionName("login")]
        [HttpPost]
        public async Task<IActionResult> UserLogin(string user, string pass)
        {
            var data = HttpContext.Session.GetString("username");
            if (HttpContext.Session.Get("username") != null) { return RedirectToAction("student", "Admin"); }
            var login = new Models.AdminModel().UserLogin(user, pass);
            if (login)
            {
                HttpContext.Session.SetString("username", user);
                //return RedirectToAction("student", "Admin");
            }
            
                dynamic response = new
                {
                    status = login,
                    message = login ? "Login Success" : "Login Failed",
                    data = ""
                };
                return Json(response);
            
        }
        [ActionName("logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");


            return RedirectToAction("Index", "Home");

        }
        [ActionName("student")]
        [HttpGet]
        public IActionResult StudentView()
        {
            if (HttpContext.Session.Get("username") == null) { return RedirectToAction("index", "Home"); }
            
                return View("StudentView");
            
        }
    }
}
