﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UnInfo.Controllers
{
    public class FileUploadController : Controller
    {
        private readonly IWebHostEnvironment _env;
        public FileUploadController(IWebHostEnvironment env)
        {
            _env = env;
        }
        // GET: /<controller>/
        [Route("fileuploadernilai")]
        [HttpPost]
        [RequestSizeLimit(2  * 1024 * 1024)]
        public async Task<IActionResult> Index(IList<IFormFile> files)
        {
            
                foreach (IFormFile source in files)
                {
                    var requestFormData = Request.Form;
                    string filename = ContentDispositionHeaderValue.Parse(source.ContentDisposition).FileName.Trim('"');
                    var newFilename = requestFormData["filenamechange"];
                    filename = this.EnsureCorrectFilename(filename);
                    var ext = Path.GetExtension(filename);
                    newFilename += ext;

                    using (FileStream output = System.IO.File.Create(this.GetPathAndFilename("nilai", newFilename)))
                    {
                        await source.CopyToAsync(output);
                    }

                }
            
            

            return new  OkResult();
        }

        private string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains("\\"))
                filename = filename.Substring(filename.LastIndexOf("\\") + 1);

            return filename;
        }

        private string GetPathAndFilename(string ext,string filename)
        {
            
            return _env.WebRootPath + "/uploads/"+ext+"/" + filename;
        }
    }
}
