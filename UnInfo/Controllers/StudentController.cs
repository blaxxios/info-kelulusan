﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.DataProtection;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UnInfo.Controllers
{
    public class StudentController : Controller
    {
        IDataProtector dataProtector;
        private readonly IWebHostEnvironment _env;
        public StudentController(IWebHostEnvironment env, IDataProtectionProvider provider)
        {
            _env = env;
            dataProtector = provider.CreateProtector(GetType().FullName);
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [Route("getstudentlist")]
        [HttpPost]
        public async Task<IActionResult> GetStudentList()
        {
            if (HttpContext.Session.Get("username") == null) { return RedirectToAction("index", "Home"); }
            var requestFormData = Request.Form;
            var search = requestFormData["search[value]"];
            var offset = requestFormData["start"];
            var length = requestFormData["length"];
            var studentList = new Models.StudentModel().GetStudents(search,offset, length);
            var studentCount = new Models.StudentModel().GetStudentCount(search);
            studentList.ForEach(x => x.Id = dataProtector.Protect(x.Id));
            dynamic response = new
            {
                Data = studentList,
                Draw = requestFormData["draw"],
                RecordsFiltered = studentCount,
                RecordsTotal = studentCount
            };

            return Ok(response);
        }

        [Route("studentadd")]
        [HttpPost]
        public async Task<IActionResult> StudentAdd(string nipd,string nisn,string nama,string gender,string kelas,string keterangan,string filepath)
        {
            if (HttpContext.Session.Get("username") == null) { return RedirectToAction("index", "Home"); }
            var studentModel = new Models.StudentModel();
            var result= studentModel.AddStudent(new Models.StudentModel() {
                NIPD=nipd,
                NISN=nisn,
                Nama=nama,
                Gender=gender,
                Kelas=kelas,
                Keterangan=keterangan,
                Filepath=filepath
            });
            dynamic response = new
            {
                status = result,
                message = result?"Add data success":"Add data failed",
                data = ""
            };
            return Json(response);
        }
        [Route("studentupdate")]
        [HttpGet]
        public async Task<IActionResult> StudentUpdateView(string id)
        {
            if (HttpContext.Session.Get("username") == null) { return RedirectToAction("index", "Home"); }
            var studentModel = new Models.StudentModel();
            var result = studentModel.GetStudentById(dataProtector.Unprotect(id));
            result.Id = dataProtector.Protect(result.Id);
            dynamic response = new
            {
                status = 1, 
                message = "",
                data = result
            };
            return Json(response);
        }

        [Route("studentupdate")]
        [HttpPost]
        public async Task<IActionResult> StudentUpdate(string id,string nipd, string nisn, string nama, string gender, string kelas, string keterangan, string filepath)
        {
            if (HttpContext.Session.Get("username") == null) { return RedirectToAction("index", "Home"); }
            var studentModel = new Models.StudentModel();
            var result = studentModel.UpdateStudent(new Models.StudentModel()
            {
                Id= dataProtector.Unprotect(id),
                NIPD = nipd,
                NISN = nisn,
                Nama = nama,
                Gender = gender,
                Kelas = kelas,
                Keterangan = keterangan,
                Filepath = filepath
            });
            dynamic response = new
            {
                status = result,
                message = result ? "Update Data Success" : "Update Data Failed",
                data = ""
            };
            return Json(response);
        }

        [Route("studentdelete")]
        [HttpPost]
        public async Task<IActionResult> StudentDelete(string id)
        {
            if (HttpContext.Session.Get("username") == null) { return RedirectToAction("index", "Home"); }
            var studentModel = new Models.StudentModel();
            var result = studentModel.DeleteStudent(new Models.StudentModel()
            {
                Id = dataProtector.Unprotect(id),
            });
            dynamic response = new
            {
                status = result,
                message = result ? "Delete Data Success" : "Delete Data Failed",
                data = ""
            };
            return Json(response);
        }

        [ActionName("studentsearchview")]
        [Route("studentsearchview")]
        [HttpPost]
        public async Task<IActionResult> StudentByNisn(string nisn)
        {

            var studentModel = new Models.StudentModel();
            var result = studentModel.GetStudentByNisn(nisn);
            if (!string.IsNullOrEmpty(result.Id))
            {
                result.Filepath = dataProtector.Protect(result.Filepath);
                return View("Index", result);
            }
            else
            {
                TempData["ErrorMessage"] = "Nis "+nisn+" tidak ditemukan !!";
                return RedirectToAction("Index", "Home");
            }
           // return new OkResult();
        }

        [ActionName("studentsearchview")]
        [Route("studentsearchview")]
        [HttpGet]
        public async Task<IActionResult> StudentByNisnGet()
        {
            return RedirectToAction("Index", "Home");
        }

        [Route("download/{filename}")]
        [HttpGet]
        public async Task<IActionResult> DownloadNilai(string filename)
        {
            //var completeUrl = HttpContext.Request.Host;
            filename = dataProtector.Unprotect(filename);
                var completeUrl = "/uploads/nilai/" + filename;
                byte[] fileBytes = System.IO.File.ReadAllBytes(_env.WebRootPath + completeUrl);
                return File(fileBytes, "application/force-download", filename);
          
            

        }
        [Route("checkfile")]
        [HttpPost]
        public async Task<IActionResult> CheckFile(string filename)
        {
            //var completeUrl = HttpContext.Request.Host;
            try
            {
                filename = dataProtector.Unprotect(filename);
                var completeUrl = "/uploads/nilai/" + filename;
                if(System.IO.File.Exists(_env.WebRootPath + completeUrl))
                {
                    dynamic response = new
                    {
                        status = 1,
                        message = Url.RouteUrl("download") +"/download/"+dataProtector.Protect(filename),
                        data = ""
                    };
                    return Json(response);
                }
                else
                {
                    dynamic response = new
                    {
                        status = 0,
                        message = "Error,File Not Found !!!",
                        data = ""
                    };
                    return Json(response);
                }
                
            }
            catch (Exception ex)
            {
                dynamic response = new
                {
                    status = 0,
                    message = "Error,File Not Found !!!",
                    data = ""
                };
                return Json(response);
            }


        }

    }
}
