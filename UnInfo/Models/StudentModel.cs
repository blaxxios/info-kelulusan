﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UnInfo.Models
{
    public class StudentModel
    {
        public string Id { get; set; }
        public string NIPD { get; set; }
        public string NISN { get; set; }
        public string Nama { get; set; }
        public string Gender { get; set; }
        public string Kelas { get; set; }
        public string Keterangan { get; set; }
        public string Filepath { get; set; }
        
        public StudentModel()
        {
        }
        public List<StudentModel> GetStudents(string search,string offset, string length)
        {
            List<StudentModel> data = new List<StudentModel>();
            try
            {
                using (var dbConn = new Connection().conn)
                {

                    var sqlString = " select * from siswa where nama like '%"+search+ "%' or nisn like '%" + search + "%' or nipd like '%" + search + "%' or keterangan like '%" + search + "%' or gender like '%" + search + "%'" +
                                    " limit " + offset + "," + length;

                    dbConn.Open();
                    var commandSql = new Connection().command;
                    //select * from gps_location_history where gps_time::timestamp::date ='2019-12-05' 
                    commandSql.CommandText = sqlString;
                    commandSql.Connection = dbConn;
                    var dataReader = commandSql.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            data.Add(new StudentModel()
                            {
                                Id = dataReader["id"].ToString(),
                                NIPD = dataReader["nipd"].ToString(),
                                NISN = dataReader["nisn"].ToString(),
                                Nama = dataReader["nama"].ToString(),
                                Gender = dataReader["gender"].ToString().ToUpper()=="L"?"Laki-laki":"Perempuan",
                                Kelas = dataReader["kelas"].ToString(),
                                Keterangan = dataReader["keterangan"].ToString().ToUpper()=="1"?"LULUS":"TIDAK LULUS",
                                Filepath = dataReader["file_path"].ToString(),

                            });



                        }

                    }

                }
            }

            catch (Exception)
            {

                throw;
            }
            return data;
        }
        public int GetStudentCount(string search)
        {
            var dataCount = 0;
            try
            {
                using (var dbConn = new Connection().conn)
                {

                    var sqlString = " select count(id) dataCount from siswa where nama like '%" + search + "%' or nisn like '%" + search + "%' or nipd like '%" + search + "%' or keterangan like '%" + search + "%' or gender like '%" + search + "%'";              
                    dbConn.Open();
                    var commandSql = new Connection().command;
                    //select * from gps_location_history where gps_time::timestamp::date ='2019-12-05' 
                    commandSql.CommandText = sqlString;
                    commandSql.Connection = dbConn;
                    var dataReader = commandSql.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {

                            dataCount = Convert.ToInt32(dataReader["dataCount"].ToString());


                        }

                    }

                }
            }

            catch (Exception)
            {

                throw;
            }
            return dataCount;
        }
        public StudentModel GetStudentByNisn(string nisn)
        {
            StudentModel data = new StudentModel();
            try
            {
                using (var dbConn = new Connection().conn)
                {

                    var sqlString = " select * from siswa where nisn='"+(string.IsNullOrEmpty(nisn)?nisn: MySql.Data.MySqlClient.MySqlHelper.EscapeString(nisn))+"'";
                                    

                    dbConn.Open();
                    var commandSql = new Connection().command;
                    //select * from gps_location_history where gps_time::timestamp::date ='2019-12-05' 
                    commandSql.CommandText = sqlString;
                    commandSql.Connection = dbConn;
                    var dataReader = commandSql.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {

                            data.Id = dataReader["id"].ToString();
                            data.NIPD = dataReader["nipd"].ToString();
                            data.NISN = dataReader["nisn"].ToString();
                            data.Nama = dataReader["nama"].ToString();
                            data.Gender = dataReader["gender"].ToString().ToUpper() == "L" ? "Laki-laki" : "Perempuan";
                            data.Kelas = dataReader["kelas"].ToString();
                            data.Keterangan = dataReader["keterangan"].ToString().ToUpper() == "1" ? "LULUS" : "TIDAK LULUS";
                            data.Filepath =Path.GetFileName(dataReader["file_path"].ToString());

                            



                        }

                    }

                }
            }

            catch (Exception)
            {

                throw;
            }
            return data;
        }
        public StudentModel GetStudentById(string id)
        {
            StudentModel data = new StudentModel();
            try
            {
                using (var dbConn = new Connection().conn)
                {

                    var sqlString = " select * from siswa where id='" + id + "'";


                    dbConn.Open();
                    var commandSql = new Connection().command;
                    //select * from gps_location_history where gps_time::timestamp::date ='2019-12-05' 
                    commandSql.CommandText = sqlString;
                    commandSql.Connection = dbConn;
                    var dataReader = commandSql.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {

                            data.Id = dataReader["id"].ToString();
                            data.NIPD = dataReader["nipd"].ToString();
                            data.NISN = dataReader["nisn"].ToString();
                            data.Nama = dataReader["nama"].ToString();
                            data.Gender = dataReader["gender"].ToString().ToUpper();
                            data.Kelas = dataReader["kelas"].ToString();
                            data.Keterangan = dataReader["keterangan"].ToString().ToUpper();
                            data.Filepath = dataReader["file_path"].ToString();





                        }

                    }

                }
            }

            catch (Exception)
            {

                throw;
            }
            return data;
        }
        public bool AddStudent(StudentModel student)
        {
            var result = false;
            try
            {
                using (var dbConn = new Connection().conn)
                {
                    var sqlString = "Insert into siswa(nipd,nisn,nama,gender,kelas,keterangan,file_path) values('"+ MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.NIPD)+ "','" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.NISN) + "','" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Nama) + "','" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Gender) + "','" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Kelas) + "'," + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Keterangan) + ",'" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Filepath) + "')";
                    dbConn.Open();
                    var commandSql = new Connection().command;
                    // select * from gps_location_history where gps_time::timestamp::date ='2019-12-05' //
                    commandSql.CommandText = sqlString;
                    commandSql.Connection = dbConn;
                    result = (commandSql.ExecuteNonQuery() == 1);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public bool UpdateStudent(StudentModel student)
        {
            var result = false;
            try
            {
                using (var dbConn = new Connection().conn)
                {
                    var sqlString = "Update siswa set nipd='" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.NIPD) + "',nisn='" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.NISN) + "',nama='" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Nama) + "',gender='" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Gender) + "',kelas='" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Kelas) + "',keterangan=" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Keterangan) + ",file_path='" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(student.Filepath) + "' where id='" + student.Id + "'";
                    dbConn.Open();
                    var commandSql = new Connection().command;
                    // select * from gps_location_history where gps_time::timestamp::date ='2019-12-05' //
                    commandSql.CommandText = sqlString;
                    commandSql.Connection = dbConn;
                    result = (commandSql.ExecuteNonQuery() == 1);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public bool DeleteStudent(StudentModel student)
        {
            var result = false;
            try
            {
                using (var dbConn = new Connection().conn)
                {
                    var sqlString = "Delete from siswa where id='" + student.Id + "'";
                    dbConn.Open();
                    var commandSql = new Connection().command;
                    // select * from gps_location_history where gps_time::timestamp::date ='2019-12-05' //
                    commandSql.CommandText = sqlString;
                    commandSql.Connection = dbConn;
                    result = (commandSql.ExecuteNonQuery() == 1);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
